# Repo M1 info

## AllSensors
Script d'exemple avec différents capteurs Groove/Seeds. Différents protocoles de communications sont illustrés : analogique, I2C et SPI.
Attention, les bibliothèques fournies par Seed sont assez mal codées.

## Cafetière
### 01 - Code basique
Le script attend de recevoir quelquechose par le port série entre les actions

### 02 - Détection du clignotement 
Le script attend de recevoir quelquechose par le port série avant de démarrer. Après la mise en route et la demande de café, il attend que les LEDs arrêtent de cligoter. 

### 03 - Détéction de l'état
Lorsque l'Arduino démarre, il ne connait pas l'état de la cafetière. 
Ajouter une portion de code pour déterminer si la cafetière est en marche, arrêtée en chauffe ou en train de servir. Pour cela, lire l'état des LEDs.

### 04 - Menu série
Afficher un choix à l'utilisateur. A l'arrêt, il doit taper `ON` pour allumer la cafetière. Une fois la phase de chauffe terminée, il doit pouvoir choisir entre `OFF` pour éteindre, `court` pour un café court et `long` pour un long. Une fois le café servi, revenir au menu de choix.

Le code suivant peut être utile ...
```cpp
String command;
 
void setup() {
    Serial.begin(9600); 
}
 
void loop() {
    if(Serial.available()){
        command = Serial.readStringUntil('\n');
         
        if(command.equals("init")){
            initialize();
        }
        else if(command.equals("send")){
            send_message();
        }
        else if(command.equals("data")){
            get_data();
        }
        else if(command.equals("reboot")){
            reboot();
        }
        else{
            Serial.println("Invalid command");
        }
    }
}
```

### 05 - Amélioration de la détection du clignotement
Le code utilisé pour la détection est un peu rustique. En utilisant les interruptions, il est possible d'enregistrer l'instant du dernier basculement d'une LED. En le comparant avec la valeur de `millis()`, il est possible de déterminer si les LEDs clignotent ou non.
Vous allez avoir besoin de https://github.com/NicoHood/PinChangeInterrupt

Les fragments de code ci-dessous peuvent être utile ...
```cpp
// Inclure la librarie
#include "PinChangeInterrupt.h"

// Variable volatile (peut être changée dans une interruption)
volatile unsigned long lastSwitch;

// Exectute led_int à chaque changement de PIN_LED_G
attachPCINT(PIN_LED_G, led_int, CHANGE);

// Fonction à executer à chaque interruption
void led_int() {
    // Enregistre l'instant actuel dans lastSwitch
    lastSwitch = millis();
}

// Attend tant que l'état des LED n'a pas changé depuis 5 sec
lastSwitch = millis();
while (millis() - lastSwitch < 5000){}
```

### 06 - Pinaillage sur le clignotement
Pour complexifier la chose, et surtout pout s'entrainer à utiliser les timers, on peut se passer de `millis()` et utiliser un timer de la puce ATmega à la place. Il va falloir potasser la [datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf) pages 74 à 134 (oui, c'est long !)

Les fragments de code ci-dessous peuvent être utile ...
```cpp
#define BIT_SET(a,b) ((a) |= (1ULL<<(b)))
#define BIT_CLEAR(a,b) ((a) &= ~(1ULL<<(b)))

// Met le bit CS00 du registre TCCR0B sur 1 : lance le timer 0 sans prescaler (16 MHz)
BIT_SET(TCCR0B, CS00);
```
