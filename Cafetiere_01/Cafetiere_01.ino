#define PIN_LED_G 2
#define PIN_LED_M 3
#define PIN_LED_D 4
#define PIN_COURT 5
#define PIN_ON 6
#define PIN_LONG 7



void setup() {
  Serial.begin(9600);
  pinMode(PIN_LED_G, INPUT);
  pinMode(PIN_LED_M, INPUT);
  pinMode(PIN_LED_D, INPUT);
  pinMode(PIN_COURT, OUTPUT);
  pinMode(PIN_ON, OUTPUT);
  pinMode(PIN_LONG, OUTPUT);
  delay(1000);
}

void loop() {
  // Attendre un message sur le port série
  Serial.println("En attente...");
  while (!Serial.available()) {
  }
  while (Serial.available()) {
    Serial.read();
  }

  // "Appuyer" sur ON
  Serial.println("Démarrage...");
  digitalWrite(PIN_ON, HIGH);
  delay(300);
  digitalWrite(PIN_ON, LOW);

  // Attendre un message sur le port série
  Serial.println("En chauffe...");
  while (!Serial.available()) {
  }
  while (Serial.available()) {
    Serial.read();
  }

  // "Appuyer" sur café court
  Serial.println("Café court");
  digitalWrite(PIN_COURT, HIGH);
  delay(300);
  digitalWrite(PIN_COURT, LOW);



}
