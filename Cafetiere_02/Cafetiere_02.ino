#define PIN_LED_G 2
#define PIN_LED_M 3
#define PIN_LED_D 4
#define PIN_COURT 5
#define PIN_ON 6
#define PIN_LONG 7



void setup() {
  Serial.begin(9600);
  pinMode(PIN_LED_G, INPUT);
  pinMode(PIN_LED_M, INPUT);
  pinMode(PIN_LED_D, INPUT);
  pinMode(PIN_COURT, OUTPUT);
  pinMode(PIN_ON, OUTPUT);
  pinMode(PIN_LONG, OUTPUT);
  delay(1000);
}

void loop() {
  int ledState, ledStateOld;
  unsigned long timeOut;

  // Attendre un message sur le port série
  Serial.println("En attente...");
  while (!Serial.available()) {
  }
  while (Serial.available()) {
    Serial.read();
  }

  // "Appuyer" sur ON
  Serial.println("Démarrage...");
  digitalWrite(PIN_ON, HIGH);
  delay(300);
  digitalWrite(PIN_ON, LOW);

  // Attendre que les LEDS ne clignottent plus
  Serial.println("En chauffe...");
  ledState = digitalRead(PIN_LED_M);
  ledStateOld = !ledState;
  timeOut = millis() + 5000;
  while (millis() < timeOut) {
    ledStateOld = ledState;
    ledState = digitalRead(PIN_LED_M);
    if (ledState != ledStateOld) {
      timeOut = millis() + 5000;
    }
    delay(250);
  }
  Serial.println("Chauffe terminée");
  delay(2000);

  // "Appuyer" sur café court
  Serial.println("Café court");
  digitalWrite(PIN_COURT, HIGH);
  delay(300);
  digitalWrite(PIN_COURT, LOW);


  // Attendre que les LEDS ne clignottent plus
  Serial.println("Ca coule...");
  ledState = digitalRead(PIN_LED_M);
  ledStateOld = !ledState;
  timeOut = millis() + 5000;
  while (millis() < timeOut) {
    ledStateOld = ledState;
    ledState = digitalRead(PIN_LED_M);
    if (ledState != ledStateOld) {
      timeOut = millis() + 5000;
    }
    delay(250);
  }
  Serial.println("Café servi");
  delay(2000);
}
