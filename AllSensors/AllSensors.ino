// BMP280
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>
#define BMP_CS   (7)
Adafruit_BMP280 bmp(BMP_CS); // hardware SPI


// DHT
#include "DHT.h"
#define DHTPIN 2     // Digital pin connected to the DHT sensor
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE); // Initialize DHT sensor.

// Groove High temp sensor
#include "High_Temp.h" //https://github.com/Seeed-Studio/Grove_HighTemp_Sensor
HighTemp ht(A2, A1);


// HP206C
#include <Wire.h>
#include <HP206C.h> // https://github.com/ncdcommunity/Arduino_Library_HP206C_24Bit_ADC_Barometer_Altimeter_Sensor
HP206C hp;


// DallasTemperature
#include <DallasTemperature.h>
#include <OneWire.h>
#define KY001_Signal_PIN 5
OneWire oneWire(KY001_Signal_PIN);
DallasTemperature sensors(&oneWire);

// Grove temp sensor
#include <math.h>
#define  B   4275                // B value of the thermistor 
#define GROVE_TEMP_PIN   A0      // Grove - Temperature Sensor connect to A0

// BME680
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"
Adafruit_BME680 bme; // I2C



void setup() {
  Serial.begin(9600);

  // BMP280
  if (!bmp.begin()) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }

  /* Default settings from datasheet. */
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
  // DHT
  dht.begin();

  // Groove High temp sensor
  ht.begin();

  // HP206
  hp.getAddr_HP206C(HP206C_DEFAULT_ADDRESS);          // 0x76
  hp.setOSR(OSR_4096);            // OSR=4096
  hp.begin();

  // DallasTemperature
  sensors.begin();


  // BME680
  if (!bme.begin()) {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
  }
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms
}

void loop() {
  Serial.println(F("---------  Temperatures ----------"));  
  // Grove temp sensor
  int a = analogRead(GROVE_TEMP_PIN);
  float R = 1023.0 / a - 1.0;
  float temperature = 1.0 / (log(R) / B + 1 / 298.15) - 273.15; // convert to temperature via datasheet
  Serial.print(F("Groove           = "));
  Serial.print(temperature);
  Serial.println(" °C");

  
  // Groove High temp sensor
  Serial.print(F("Groove High Temp = "));
  Serial.print(ht.getThmc());
  Serial.println(" °C");


  // DallasTemperature
  sensors.requestTemperatures();
  Serial.print(F("DS18B20          = "));
  Serial.print(sensors.getTempCByIndex(0));
  Serial.println(" °C");

  
  // BMP280
  Serial.println(F("------------- BMP280 -------------"));
  Serial.print(F("Temperature      = "));
  Serial.print(bmp.readTemperature());
  Serial.println(" *C");
  Serial.print(F("Pressure         = "));
  Serial.print(bmp.readPressure() / 100);
  Serial.println(" hPa");
  Serial.print(F("Approx. Altitude = "));
  Serial.print(bmp.readAltitude(1013.25)); /* Adjusted to local forecast! */
  Serial.println(" m"); 
  

  // DHT
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float hic = dht.computeHeatIndex(t, h, false);
  Serial.println(F("------------- DHT11 --------------"));
  Serial.print(F("Humidity         = "));
  Serial.print(h);
  Serial.println(F("%"));
  Serial.print(F("Temperature      = "));
  Serial.print(t);
  Serial.println(F("°C"));
  Serial.print(F("Heat index       = "));
  Serial.print(hic);
  Serial.println(F("°C ")); 


  // HP206
  Serial.println(F("------------- HP206C -------------"));
  hp.Measure_Sensor();
  Serial.print(F("Pressure         = "));
  Serial.print(hp.hp_sensorData.P);
  Serial.println(" hPa");
  Serial.print(F("Approx. Altitude = "));
  Serial.print(hp.hp_sensorData.A);
  Serial.println(" m");
  Serial.print(F("Temperature      = "));
  Serial.print(hp.hp_sensorData.T);
  Serial.println(" °C"); 
 

  // BME680
  bme.performReading();
  Serial.println(F("------------ BME680 --------------"));
  Serial.print(F("Temperature      = "));
  Serial.print(bme.temperature);
  Serial.println(" *C");
  Serial.print(F("Pressure         = "));
  Serial.print(bme.pressure / 100.0);
  Serial.println(" hPa");
  Serial.print(F("Humidity         = "));
  Serial.print(bme.humidity);
  Serial.println(" %");
  Serial.print("Gas resistance   = ");
  Serial.print(bme.gas_resistance / 1000.0);
  Serial.println(" KOhms");
  Serial.print(F("Approx. Altitude = "));
  Serial.print(bme.readAltitude(1013.25));
  Serial.println(" m");
  

  // Fin de boucle
  Serial.println("----------------------------------");
  Serial.println();
  Serial.println();
  delay(2000);

}
